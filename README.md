# To start:
1. Run `npm install` in your terminal
2. Duplicate `.env.example`, rename copy to `.env` and replace `VITE_OPENAI_API_KEY` with your OpenAI API key from [The OpenAI Platform](https://platform.openai.com/account/api-keys)
3. Run `npm start` in your terminal
4. Open http://localhost:5173 in your browser

## Does the bot not respond?
A "429 Too Many Requests" error might indicate that you don't have [free credits](https://platform.openai.com/account/usage). In that case, you have to add a payment method. Using this chat bot should be relatively cheap, but check the [pricing information](https://openai.com/pricing) and use at your own discretion. You may need to pay for $5 initial credits to start using the API.

# What now?
1. Try to change the `SYSTEM_PROMPT` in the `openai.ts` file as you talk with the bot
2. (Intermediate) Limit the number of messages sent to GPT to stay below the token context limit
3. (Advanced) Can you implement speech-to-text with a microphone button and [Whisper](https://platform.openai.com/docs/api-reference/audio)?

# Tips:
- Read [OpenAI's official GPT best practices](https://platform.openai.com/docs/guides/gpt-best-practices).
- Read [OpenAI's API reference](https://platform.openai.com/docs/api-reference/)