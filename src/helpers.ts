import {
  MessageImageContentProps,
  MessageProps,
} from "@chatscope/chat-ui-kit-react";
import OpenAI from "openai";

type ChatCompletionMessageType =
  | OpenAI.Chat.Completions.ChatCompletionSystemMessageParam
  | OpenAI.Chat.Completions.ChatCompletionUserMessageParam
  | OpenAI.Chat.Completions.ChatCompletionAssistantMessageParam;

type ChatCompletionRole = "assistant" | "user" | "system";

export const messagePropsToGPTMessage = (
  messageProps: MessageProps
): ChatCompletionMessageType => {
  if (!messageProps.model)
    throw new Error("Invalid messageProps format: " + messageProps);

  // If the message has an image payload, capture the alt text
  const messagePayload = messageProps.model.payload as MessageImageContentProps;
  const imageAltText = messagePayload?.alt ? messagePayload.alt : "";

  return {
    role:
      messageProps?.model?.sender &&
      ["assistant", "user", "system"].includes(messageProps.model.sender)
        ? (messageProps.model.sender as ChatCompletionRole) // in this case, it's a valid role
        : "assistant",
    content: messageProps.model?.message ?? imageAltText ?? "",
  };
};

export const formatUserMessage = (text: string): MessageProps => {
  return {
    model: {
      message: text,
      sender: "user",
      direction: "outgoing",
      position: "normal",
    },
  };
};

export const formatAssistantMessage = (text: string): MessageProps => {
  return {
    model: {
      message: text,
      sender: "assistant",
      direction: "incoming",
      position: "normal",
    },
  };
};

export const formatAssistantImageMessage = (
  src: string,
  alt: string
): MessageProps => {
  return {
    // Add "image" type to render "payload" as an image
    type: "image",
    model: {
      sender: "assistant",
      direction: "incoming",
      position: "normal",
      payload: {
        src,
        alt,
        width: "512px",
      },
    },
  };
};
