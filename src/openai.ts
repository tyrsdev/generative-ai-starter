import { MessageProps } from "@chatscope/chat-ui-kit-react";
import OpenAI from "openai";
import { messagePropsToGPTMessage } from "./helpers";

const openai = new OpenAI({
  apiKey: import.meta.env.VITE_OPENAI_API_KEY,
  // Allow running in browser only during development
  // The API key should not be included if this chat is deployed to production
  dangerouslyAllowBrowser: import.meta.env.DEV,
});

const SYSTEM_PROMPT = "You are a helpful AI assistant.";
// const SYSTEM_PROMPT = "You are a professional digital image creator.";
// const SYSTEM_PROMPT = "You are a teenager who giggles a lot.";
// const SYSTEM_PROMPT = "You are a grumpy old man.";
// const SYSTEM_PROMPT = "You are a sarcastic best friend.";
// const SYSTEM_PROMPT = "You are an anxious nurse.";

export const askGPT = async (
  messages: MessageProps[],
  functionMessage?: OpenAI.Chat.ChatCompletionFunctionMessageParam
): Promise<OpenAI.Chat.Completions.ChatCompletionMessage> => {
  // Convert chat format (MessageProps) to GPT format (ChatCompletionRequestMessage)
  const gptMessages: OpenAI.Chat.ChatCompletionMessageParam[] = messages.map(
    messagePropsToGPTMessage
  );

  if (functionMessage) gptMessages.push(functionMessage);

  // Request a GPT chat completion
  const result = await openai.chat.completions.create({
    model: "gpt-3.5-turbo",
    messages: [
      {
        role: "system",
        content: "Follow all instructions. Stay in character. " + SYSTEM_PROMPT,
      },
      ...gptMessages,
    ],
    functions: [
      {
        name: "sendImage",
        description: "Describe the image that will be sent",
        parameters: {
          type: "object",
          properties: {
            imageDescription: {
              type: "string",
              description:
                "A declarative description of the image that will be sent",
            },
          },
          required: ["imageDescription"],
        },
      },
    ],
    // If it just called a function, stop there, otherwise let GPT decide
    function_call: functionMessage ? "none" : "auto",
  });

  // Uncomment the line below to learn more about the API results:
  // console.log(result);

  if (!result.choices[0].message)
    throw new Error("Chat Completion generation failed.");

  // Return the first message's content (text)
  return result.choices[0].message;
};

export const askDALLE = async (prompt: string): Promise<string> => {
  // Generate an image, only "prompt" (text) is required.
  const result = await openai.images.generate({
    model: "dall-e-2",
    prompt,
    size: "512x512",
    n: 1,
    response_format: "url",
  });

  // Uncomment the line below to learn more about the API results:
  // console.log(result);

  if (!result.data[0].url) throw new Error("Image generation failed.");

  // Return the first image's url (update if you make edits to "n" or "response_format" above)
  return result.data[0].url;
};

type GenerateImageArgs = {
  imageDescription: string;
};

type GenerateImageResult = {
  url: string;
  description: string;
};

type HandleFunctionResult = {
  sendImage?: GenerateImageResult;
};

export const handleFunctionCall = async (
  functionCall: OpenAI.Chat.ChatCompletionMessage.FunctionCall
): Promise<HandleFunctionResult> => {
  const availableFunctions = ["sendImage"];
  const functionName = functionCall.name;
  if (!functionName || !availableFunctions.includes(functionName))
    throw new Error("Invalid functionName");

  const functionArgs = functionCall.arguments;
  if (!functionArgs) throw new Error("Missing required arguments.");

  if (functionName == "sendImage") {
    const parsedArgs = JSON.parse(functionArgs) as GenerateImageArgs;
    const generatedImageUrl = await askDALLE(parsedArgs.imageDescription);
    return {
      sendImage: {
        url: generatedImageUrl,
        description: parsedArgs.imageDescription,
      },
    };
    // Try implementing other functions to call!
    // } else if (functionName == "...") {
    //   ...
  } else throw new Error(`Unhandled function: ${functionName}`);
};
