import "@chatscope/chat-ui-kit-styles/dist/default/styles.min.css";
import "./App.css";
import {
  ChatContainer,
  MainContainer,
  Message,
  MessageInput,
  MessageList,
  MessageProps,
  TypingIndicator,
} from "@chatscope/chat-ui-kit-react";
import { useState } from "react";
import { FormEvent } from "react";
import { askGPT, handleFunctionCall } from "./openai";
import {
  formatAssistantImageMessage,
  formatAssistantMessage,
  formatUserMessage,
} from "./helpers";
import OpenAI from "openai";

function App() {
  const [input, setInput] = useState("");
  const [messages, setMessages] = useState<MessageProps[]>([]);

  // Track when the bot is typing for the TypingIndicator
  const [botIsTyping, setBotIsTyping] = useState(false);

  const handleSend = async () => {
    // Add user input to chat view
    const userMessage = formatUserMessage(input);
    setMessages((prev) => [...prev, userMessage]);

    try {
      setBotIsTyping(true);
      // Request a response which may contain a function call or text content
      const response = await askGPT([...messages, userMessage]);
      if (response.function_call) {
        // Destruct more function call results if more function options are implemented
        const { sendImage } = await handleFunctionCall(response.function_call);
        if (sendImage) {
          // Extract the result of the function call
          const { url, description } = sendImage;

          // Format the result in a GPT message so that the bot can speak about it
          const functionResultMessage: OpenAI.Chat.ChatCompletionFunctionMessageParam =
            {
              role: "function",
              name: "sendImage",
              content: `Image: ${description} (from sendImage)`,
            };
          try {
            setBotIsTyping(true);
            const response = await askGPT(
              [...messages, userMessage],
              functionResultMessage
            );
            const answer = response.content;

            setMessages((prev) => [
              ...prev,
              // Conditionally add the response's content before the image if the bot responded with text content
              ...(answer ? [formatAssistantMessage(answer)] : []),
              formatAssistantImageMessage(url, description),
            ]);
          } finally {
            setBotIsTyping(false);
          }
        }
      } else if (response.content) {
        const answer = response.content;
        setMessages((prev) => [...prev, formatAssistantMessage(answer)]);
      }
    } finally {
      setBotIsTyping(false);
    }
  };

  const handleInput = (e: FormEvent<HTMLDivElement>) => {
    e.preventDefault();
    setInput(e.currentTarget.innerText);
  };

  return (
    <div style={{ position: "relative", width: "800px", height: "100vh" }}>
      <MainContainer>
        <ChatContainer>
          <MessageList
            typingIndicator={botIsTyping ? <TypingIndicator /> : null}
          >
            {messages.map((messageProps, i) => (
              <Message key={i} {...messageProps} />
            ))}
          </MessageList>
          <MessageInput
            placeholder="Type message here"
            autoFocus
            attachButton={false}
            onSend={handleSend}
            onInput={handleInput}
          />
        </ChatContainer>
      </MainContainer>
    </div>
  );
}

export default App;
